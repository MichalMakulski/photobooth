const http = require('http');
const fs = require('fs');
const port = process.env.PORT || 1337;

http.createServer((req, res) => {
  const url = req.url;

  if (url === '/') {
    fs.createReadStream(`${__dirname}/client/index.html`, 'utf8').pipe(res);
    return;
  }

  if (url.indexOf('client') !== -1) {
    const fileName = url.replace(/\/client\/(\w)/, '$1');
    fs.createReadStream(`${__dirname}/client/${fileName}`).pipe(res);
    return;
  }

}).listen(port);