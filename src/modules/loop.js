import videoToCanvas from './canvas';

export default function (e) {
  const interval = window.setInterval(takeAPhoto, 2000);
  let imageNo = 0;

  e.target.setAttribute('disabled', 'disabled');

  function takeAPhoto () {
    if (imageNo > 3) {
      handleEnd(interval);
      return;
    }
    videoToCanvas(imageNo);
    imageNo++;
  }
}

function handleEnd(interval) {
  window.clearInterval(interval);
  document.querySelector('h1').textContent = 'Click on photos to save them!';
  document.getElementById("canvas").addEventListener("click", function(e) {
    const newTab = window.open(e.target.toDataURL(), '_blank');
    newTab.focus();
  }, false);
}