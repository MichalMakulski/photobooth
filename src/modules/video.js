export default function () {
 const videoConf =  { width: 640, height: 480 }
 return navigator.mediaDevices.getUserMedia({video: videoConf})
    .then(function(stream) {
      const video = document.getElementById('video-elt');
      video.srcObject = stream;
    }).catch(function(err) {
    console.log("Video capture error: ", err);
  });
}