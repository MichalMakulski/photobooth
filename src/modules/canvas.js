export default function (imageNo) {
  const video = document.getElementById('video-elt');
  const imgWidth = 320;
  const imgHeight = 240;
  const canvas = document.getElementById("canvas");
  const context = canvas.getContext("2d");

  context.drawImage(video, 0, imageNo * imgHeight, imgWidth, imgHeight);
}   