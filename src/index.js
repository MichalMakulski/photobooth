import initVideo from './modules/video';
import startTakingPhotos from './modules/loop';

initVideo();
document.getElementById("start").addEventListener("click", startTakingPhotos, false);