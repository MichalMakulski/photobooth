module.exports = {
    entry: './src/index.js',
    output: {
        filename: './client/js/main.js'
    },
    watch: true,
    module: {
        loaders: [
          {
            test: /\.js?$/,
            exclude: /node_modules/,
            loaders: ["babel-loader"],
          },
        ],
    },
  babel: {
    presets: ['es2015']
  }
};